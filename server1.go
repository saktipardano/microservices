package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type AutoGenerated struct {
	Rajaongkir Rajaongkir `json:"rajaongkir"`
}
type Query struct {
	ID string `json:"id"`
}
type Status struct {
	Code        int    `json:"code"`
	Description string `json:"description"`
}
type Results struct {
	ProvinceID string `json:"province_id"`
	Province   string `json:"province"`
}
type Rajaongkir struct {
	Query   Query   `json:"query"`
	Status  Status  `json:"status"`
	Results Results `json:"results"`
}

func getAPI() (string, string) {
	api_key := "99f0853a6670951875a458989aeb1e4d"

	url := "https://api.rajaongkir.com/starter/city?id=39"

	req, _ := http.NewRequest("GET", url, nil)

	req.Header.Add("key", api_key)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	//fmt.Println(res)
	//fmt.Println(string(body))
	var ag AutoGenerated
	json.Unmarshal(body, &ag)
	var provinceid = ag.Rajaongkir.Results.ProvinceID
	var province = ag.Rajaongkir.Results.Province

	return provinceid, province
}

type Ongkir struct {
	ProvinceID string
	Province   string
}

func getOngkir(w http.ResponseWriter, r *http.Request) {
	var ongkir Ongkir // variable untuk memetakan data product yang terbagi menjadi 3 field

	provinceid, provincename := getAPI()

	fmt.Println(provinceid)
	fmt.Println(provincename)

	ongkir.ProvinceID = provinceid
	ongkir.Province = provincename
	json.NewEncoder(w).Encode(ongkir)

}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/getOngkir", getOngkir).Methods("GET") // menjalurkan URL untuk dapat mengkases data JSON API product ke /getproducts
	http.Handle("/", router)
	log.Fatal(http.ListenAndServe(":9000", router))
}
